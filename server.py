from flask import Flask, jsonify, abort, request, make_response, url_for
from flask_cors import CORS

app = Flask(__name__)
CORS(app) 

tasks = [ 
	{
		'id': 1,
		'title': 'Buy groceries',
		'description': 'Milk, Cheese, Pizza, Fruit, Tylenol',
		'done': False
	},
	{
		'id': 2,
		'title': 'My POC app',
		'description': 'Need to develop a POC app',
		'done': False
	}
]

@app.route("/")
def home():
    return "My flask POC app"

@app.route('/todo/api/v1.0/tasks', methods = ['GET'])
def get_tasks():
    return jsonify( { 'tasks': tasks } )
  
if __name__ == "__main__":
    app.run(debug=True) 